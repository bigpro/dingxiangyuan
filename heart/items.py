# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class HeartItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    collection = 'passages'
    #文章内容
    tag1 = scrapy.Field()
    # tag2 = scrapy.Field()
    url = scrapy.Field()
    title = scrapy.Field()
    date = scrapy.Field()
    source = scrapy.Field()
    author = scrapy.Field()
    content = scrapy.Field()
    article_id = scrapy.Field()
    tag = scrapy.Field()
